package com.example.QAapiresponse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class QaTestApiResponseApplication {

	public static void main(String[] args) {
		SpringApplication.run(QaTestApiResponseApplication.class, args);
	}

}

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException

import cucumber.api.java.en.And
import cucumber.api.java.en.Given
import cucumber.api.java.en.Then
import cucumber.api.java.en.When



class PaymentCreditCard {
	/**9
	 * The step definitions below match with Katalon sample Gherkin steps
	 */
	@Given("I want to navigate credit card payment page")
	def paymentCreditCard() {
		println ("You have reached in credit card page")
		WebUI.openBrowser('')
		WebUI.navigateToUrl('https://demo.midtrans.com/')
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_BUY NOW'))
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/div_CHECKOUT'))
		WebUI.delay(5)
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Continue'))
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Credit CardPay with Visa MasterCard JCB o_fd4abe'))
	}

	@When("fill my credit card information like card number (.*), expiry date (.*) and cvv (.*)")
	def fillCreditCard(String cardNumber, expiryDate, cvv) {
		println ("I want to fill my credit card information")
		println ("input my number credit card: "+cardNumber)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_sample-store-1582016031_cardnumber'), cardNumber)
		WebUI.delay(5)
		println ("input my expired date: "+expiryDate)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input'), expiryDate)
		WebUI.delay(5)
		println ("input my cvv: "+ cvv)
		WebUI.setText(findTestObject('Object Repository/Page_Sample Store/input_1'), cvv)
		WebUI.delay(5)
	}

	@And("process my payment and navigate to bank's OTP")
	def payNow() {
		println "Process my payment"
		WebUI.click(findTestObject('Object Repository/Page_Sample Store/a_Pay Now'))
	}

	@And("fill my OTP and process my transaction")
	def processPayment() {
		println "Process my transaction"
		WebUI.setText(findTestObject('Page_Sample Store/input_Password_PaRes'), '112233')

		WebUI.click(findTestObject('Page_Sample Store/button_OK'))
	}

	@Then("i get message my transaction success")
	def successcreditcard() {
		println "Process my transaction"
		WebUI.verifyElementPresent(findTestObject('Page_Sample Store/div_Transaction successful'), 5)
		WebUI.closeBrowser()
	}

	@Then("i get message my transaction declined by bank")
	def failedcreditcard() {
		println "Process my transaction"
		WebUI.verifyElementPresent(findTestObject('Page_Sample Store/span_Your card got declined by the bank'), 5)
		WebUI.closeBrowser()
	}
}
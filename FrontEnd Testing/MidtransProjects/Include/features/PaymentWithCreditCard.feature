#Author: your.email@your.domain.com
#Keywords Summary :
#Feature: List of scenarios.
#Scenario: Business rule through list of steps with arguments.
#Given: Some precondition step
#When: Some key actions
#Then: To observe outcomes or validation
#And,But: To enumerate more Given,When,Then steps
#Scenario Outline: List of steps for data-driven as an Examples and <placeholder>
#Examples: Container for s table
#Background: List of steps run before each of the scenarios
#""" (Doc Strings)
#| (Data Tables)
#@ (Tags/Labels):To group Scenarios
#<> (placeholder)
#""
## (Comments)
#Sample Feature Definition Template
@PaymentwithCreditCard
Feature: Payment with Credit card feature
  As user I want to pay my order with credit card so that I can finished my order transaction

  @Successed
  Scenario Outline: Success transaction with valid credit card
    Given I want to navigate credit card payment page
    When fill my credit card information like card number <card_number>, expiry date <expiry_date> and cvv <cvv>
    And process my payment and navigate to bank's OTP
    And fill my OTP and process my transaction
    Then i get message my transaction success

    Examples: 
      | card_number         | expiry_date | cvv |
      | 4811 1111 1111 1114 | 02/20       | 123 |

  @Failed
  Scenario Outline: Success transaction with valid credit card
    Given I want to navigate credit card payment page
    When fill my credit card information like card number <card_number>, expiry date <expiry_date> and cvv <cvv>
    And process my payment and navigate to bank's OTP
    And fill my OTP and process my transaction
    Then i get message my transaction declined by bank

    Examples: 
      | card_number         | expiry_date | cvv |
      | 4911 1111 1111 1113 | 02/20       | 123 |
